-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 01, 2017 at 01:47 AM
-- Server version: 5.7.16
-- PHP Version: 7.1.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cake`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `created`) VALUES
(3, 'mohammed', 'test@yahoo.com', '$2a$10$wTVmqCST4pJZs.BHQylZrOf.wCbLz/pTq9v0AKN6ihEbQY/avZzju', '2017-03-31 20:27:55'),
(4, 'rabab', 'test1@yahoo.com', '$2a$10$qhOspeVGuB6VU9EJuB6/KuCDg8zLGPVfYv836yo0ocfwWKHLxpT1K', '2017-03-31 20:37:52'),
(5, 'wafaa', 'test2@yahoo.com', '$2a$10$t8RVkgymYq8wEa.9Stt2PeK3QIS6Wt/VwguWeqUsrOTbN/8Qx.s9a', '2017-03-31 22:26:50'),
(6, 'saids', 'test3@yahoo.com', '$2a$10$p5RKFt.h.GzFTOj3xxVMMOmFzX3aznuShiEe9IdK.Qk/o3ma08RsK', '2017-03-31 23:08:19');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
