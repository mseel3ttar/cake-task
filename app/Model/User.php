<?php
App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');
class User extends AppModel {
    public $validate = array(
        'username' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'A username is required'
            ),
            'between' => array(
                'rule' => array('lengthBetween', 5, 15),
                'message' => 'Usernames must be between 5 to 15 characters'
            ),
            'unique' => array(
                'rule'    => 'isUnique',
                'message' => 'This username is already in use'
            ),
            'alphaNumeric' => array(
                'rule' => 'alphaNumeric',
                'message' => 'Username can only be letters, numbers'
            ),
        ),
        'password' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'A password is required'
            ),
            'between' => array(
                'rule' => array('lengthBetween', 5, 20),
                'message' => 'Password must be between 5 to 20 characters'
            )
        ),
        'email' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'Email is required'
            ),
            'unique' => array(
                'rule'    => 'isUnique',
                'message' => 'This email is already in use'
            ),
            'between' => array(
                'rule' => array('lengthBetween', 5, 50),
                'message' => 'Email must be between 5 to 50 characters'
            )
        )
    );


    public function beforeSave($options = array()) {
    if (isset($this->data[$this->alias]['password'])) {
        $passwordHasher = new BlowfishPasswordHasher();
        $this->data[$this->alias]['password'] = $passwordHasher->hash(
            $this->data[$this->alias]['password']
        );
    }
    return parent::beforeSave($options);
}
}
?>


